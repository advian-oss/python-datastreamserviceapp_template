# datastreamserviceapp template

Cookiecutter template for applications using https://gitlab.com/advian-oss/python-datastreamserviceapp

## Usage

  1. Install [cookiecutter](https://pypi.org/project/cookiecutter/)
  2. `cookiecutter gl:advian-oss/python-datastreamserviceapp_template`
  3. Fill in the forms
  4. Profit!

The newly generated directory structure has more information, remember init your repo first.
