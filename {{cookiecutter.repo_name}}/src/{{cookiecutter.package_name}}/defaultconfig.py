"""Default configuration file contents"""

# Remember to add tests for keys into test_{{cookiecutter.package_name}}.py
DEFAULT_CONFIG_STR = """
[zmq]
pub_sockets = ["ipc:///tmp/{{cookiecutter.package_name}}_pub.sock", "tcp://*:{%set pub_port = range(49152, 65535) | random %}{{ pub_port }}"]
rep_sockets = ["ipc:///tmp/{{cookiecutter.package_name}}_rep.sock", "tcp://*:{{ pub_port+1 }}"]

""".lstrip()
